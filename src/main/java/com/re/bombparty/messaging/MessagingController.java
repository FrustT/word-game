package com.re.bombparty.messaging;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class MessagingController {

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Message greeting(@Payload Message message) throws Exception {
        System.out.println("Received message: ");
        Thread.sleep(1000); // simulated delay
        return new Message("Hello, " + message.getName() + "!");
    }
}
